** Exercise 1

107

1

nothing, 10 is not a function

54

120

** Exercise 2

(let ((max (lambda (x y) (if (> x y) x y)))) (map (lambda (l) (max (car l) (car (cdr l)))) (list '(1 2) '(0 4) '(2 -1))))

parens: ((((()(()))))((()(()(())))(()()())))
